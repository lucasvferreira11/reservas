package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.MoedaEntradaDTO
import br.com.usp.reservas.dataclass.MoedaSaidaDTO
import br.com.usp.reservas.dataclass.MoedaViewModel
import br.com.usp.reservas.repository.MoedaRepository
import br.com.usp.reservas.util.MoedaParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class MoedaServiceImpl : MoedaService{

    @Autowired
    lateinit var moedaRepository: MoedaRepository

    override fun consultar(moedaEntradaDTO: MoedaEntradaDTO): MoedaViewModel {

        val responseLocal = moedaRepository.consultar(moedaEntradaDTO.moeda_local)
        val responsePretendida = moedaRepository.consultar(moedaEntradaDTO.moeda_pretendida)

        if(HttpStatus.OK.equals(responseLocal.statusCode) && HttpStatus.OK.equals(responsePretendida.statusCode)){
            val cotacaoLocal = responseLocal.body ?: arrayOf(MoedaSaidaDTO(1.0))
            val cotacaoPretendida = responsePretendida.body ?: arrayOf(MoedaSaidaDTO(1.0))
            return MoedaViewModel(
                    MoedaParser.calculaCotacao(
                            cotacaoLocal.get(0).bid.toDouble(), cotacaoPretendida.get(0).bid.toDouble(), moedaEntradaDTO.valor_cotacao))
        }
        return MoedaViewModel("0.0");

    }

}