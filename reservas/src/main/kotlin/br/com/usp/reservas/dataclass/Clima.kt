package br.com.usp.reservas.dataclass

data class Clima(val min: String, val max: String)