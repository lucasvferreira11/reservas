package br.com.usp.reservas.repository

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.ClimaRetorno
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import java.util.*

@Repository
class ClimaRepository{

    companion object {
        const val URL = "https://api.hgbrasil.com/weather?key={chave}" +
                "&fields=only_results,city_name,temp,description,currently,humidity,wind_speedy&city_name={cityname}"
    }

    fun consultar(cidadeDTO: CidadeDTO): ClimaRetorno?{
        val headers = HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        val entity = HttpEntity<String>("parameters", headers);
        val response= RestTemplate().exchange(getUrl(cidadeDTO), HttpMethod.GET,entity, ClimaRetorno::class.java)
        return response.body;
    }

    fun getUrl(cidadeDTO: CidadeDTO): String{
        return URL.replace("{chave}", cidadeDTO.chave).replace("{cityname}",cidadeDTO.toString())
    }

}