package br.com.usp.reservas.dataclass

data class MoedaEntradaDTO(val moeda_local: String, val moeda_pretendida: String, val valor_cotacao: String)
