package br.com.usp.reservas.mapper

import br.com.usp.reservas.dataclass.PassagemEntradaDTO
import br.com.usp.reservas.util.DataHelper
import com.amadeus.Params

class PassagemEntradaMapper{

    companion object{
        fun from(passagemEntradaDTO: PassagemEntradaDTO): Params {
            return Params.with("originLocationCode", AeroportoMapper.from("SYD"))
                    .and("destinationLocationCode", AeroportoMapper.from("BKK"))
                    .and("departureDate", DataHelper.converteDMAintoAMD(passagemEntradaDTO.inicio))
                    .and("returnDate", DataHelper.converteDMAintoAMD(passagemEntradaDTO.fim))
                    .and("adults", passagemEntradaDTO.adultos)
                    .and("max", 5)
        }
    }

}