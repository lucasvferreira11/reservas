package br.com.usp.reservas.mapper

import br.com.usp.reservas.dataclass.BaseViewModel
import br.com.usp.reservas.dataclass.HotelEntradaDTO
import com.amadeus.resources.HotelOffer

class HotelSaidaMapper{

    companion object {
        fun from(hotelOffers: Array<out HotelOffer>?, hotelEntradaDTO: HotelEntradaDTO): List<BaseViewModel> {

            if(hotelOffers == null){
                return emptyList()
            }

            var lista = ArrayList<BaseViewModel>()

            hotelOffers.forEach {
                val name = it.hotel.name
                val cityCode = it.hotel.cityCode
                if(it.isAvailable){
                    it.offers.forEach {
                        var hotelOffer = BaseViewModel(1, cityCode, name,
                                hotelEntradaDTO.checkIn, hotelEntradaDTO.checkOut,
                                it.price.total, hotelEntradaDTO.adulto,"Hotel")
                        lista.add(hotelOffer)
                    }
                }
            }

            lista.forEach{
                println("item de retorno " + it)
            }

            return lista

        }
    }

}