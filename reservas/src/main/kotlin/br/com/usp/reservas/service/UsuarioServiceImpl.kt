package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.Usuario
import br.com.usp.reservas.repository.ClimaRepository
import br.com.usp.reservas.repository.UsuarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UsuarioServiceImpl: UsuarioService{

    @Autowired
    private lateinit var usuarioRepository: UsuarioRepository

    override fun cadastrar(usuario: Usuario) {
        usuarioRepository.save(usuario)
    }

    override fun consultar(email: String): Usuario {
        return usuarioRepository.findByEmail(email) ?: throw IllegalArgumentException("Usuario nao existe")
    }

    override fun validaSenha(email: String, senha: String): Boolean {
        val usuario = consultar(email)
        return usuario.senha.equals(senha)
    }

}