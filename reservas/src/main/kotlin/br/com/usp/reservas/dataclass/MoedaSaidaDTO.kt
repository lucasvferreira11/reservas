package br.com.usp.reservas.dataclass

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class MoedaSaidaDTO(var bid: Double)