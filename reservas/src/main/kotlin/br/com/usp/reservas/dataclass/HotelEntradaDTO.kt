package br.com.usp.reservas.dataclass

import java.util.*

data class HotelEntradaDTO(
        val cidade: String, val checkIn: String, val checkOut: String, val adulto: Int, val preco: Int
)