package br.com.usp.reservas

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.Clima
import br.com.usp.reservas.repository.ClimaRepository
import br.com.usp.reservas.util.DataHelper
import br.com.usp.reservas.util.MoedaParser
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ReservasApplication

fun main(args: Array<String>) {
    runApplication<ReservasApplication>(*args)
}
