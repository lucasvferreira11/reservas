package br.com.usp.reservas.repository

import okhttp3.OkHttpClient
import okhttp3.Request

class RepositoryBase{

    fun createRequest(url: String): Request {
        return Request.Builder()
                .url(url)
                .build()
    }

}