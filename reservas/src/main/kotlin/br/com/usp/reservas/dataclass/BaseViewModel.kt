package br.com.usp.reservas.dataclass

import java.util.*

data class BaseViewModel (val id_product: Int, val origin: String, val nameProduct: String,
                          val checkin: String, val checkout: String, val price: String,
                          val amount: Int, val type_product: String)