package br.com.usp.reservas.repository

import br.com.usp.reservas.consts.RepoConsts
import com.amadeus.Amadeus
import com.amadeus.Params
import com.amadeus.resources.FlightOfferSearch
import com.amadeus.resources.HotelOffer
import com.amadeus.shopping.HotelOffers
import org.springframework.stereotype.Repository

@Repository
class HotelRepository{

    fun consultar(params: Params): Array<out HotelOffer>? {
        val amadeus = Amadeus.builder(RepoConsts.ID,RepoConsts.SECRET).build();
        return amadeus.shopping.hotelOffers.get(params)
    }

}