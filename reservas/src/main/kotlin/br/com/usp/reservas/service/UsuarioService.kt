package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.Usuario

interface UsuarioService{
    fun cadastrar(usuario: Usuario)
    fun consultar(email: String): Usuario
    fun validaSenha(email: String, senha: String): Boolean
}