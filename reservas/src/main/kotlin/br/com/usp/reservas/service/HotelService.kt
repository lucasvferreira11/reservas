package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.BaseViewModel
import br.com.usp.reservas.dataclass.HotelEntradaDTO
import br.com.usp.reservas.dataclass.HotelViewModel

interface HotelService{
    fun consultar(hotelEntradaDTO: HotelEntradaDTO): List<BaseViewModel>
}