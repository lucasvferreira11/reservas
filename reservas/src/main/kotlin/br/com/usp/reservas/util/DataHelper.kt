package br.com.usp.reservas.util

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DataHelper {

    companion object {
        fun converteDMAintoAMD(dma: String): String {
            val date = LocalDate.parse(dma, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
            return date.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")).toString()
        }
    }

}