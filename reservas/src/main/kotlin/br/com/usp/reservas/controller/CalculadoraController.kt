package br.com.usp.reservas.controller

import br.com.usp.calculadora.CalculatorLocator
import br.com.usp.calculadora.CalculatorSoapStub
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/calculadora")
class CalculadoraController {

    @GetMapping("/somar")
    fun somar(@RequestParam valorA: Int, @RequestParam valorB: Int): Int{
        val stub = CalculatorSoapStub()
        val c = CalculatorLocator()
        val result = c.calculatorSoap.add(valorA,valorB);
        return result;
    }

    @GetMapping("/subtrair")
    fun subtrair(@RequestParam valorA: Int, @RequestParam valorB: Int): Int{
        val stub = CalculatorSoapStub()
        val c = CalculatorLocator()
        val result = c.calculatorSoap.subtract(valorA,valorB);
        return result;
    }

    @GetMapping("/multiplicar")
    fun multiplicar(@RequestParam valorA: Int, @RequestParam valorB: Int): Int{
        val stub = CalculatorSoapStub()
        val c = CalculatorLocator()
        val result = c.calculatorSoap.multiply(valorA,valorB);
        return result;
    }

    @GetMapping("/dividir")
    fun dividir(@RequestParam valorA: Int, @RequestParam valorB: Int): Int{
        val stub = CalculatorSoapStub()
        val c = CalculatorLocator()
        val result = c.calculatorSoap.divide(valorA,valorB);
        return result;
    }

}