package br.com.usp.reservas.controller

import br.com.usp.reservas.dataclass.BaseViewModel
import br.com.usp.reservas.dataclass.HotelEntradaDTO
import br.com.usp.reservas.dataclass.HotelViewModel
import br.com.usp.reservas.service.HotelService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/hotel")
class HotelController{

    @Autowired
    private lateinit var hotelService: HotelService

    @GetMapping("/consultar")
    fun consultar(@RequestParam cidade: String, @RequestParam checkin: String, @RequestParam checkout: String,
                    @RequestParam adulto: Int, @RequestParam preco: Int): List<BaseViewModel>{

        println("Entrou no Controller")

        var hotelEntradaDTO = HotelEntradaDTO(cidade,checkin,checkout,adulto,preco)
        return hotelService.consultar(hotelEntradaDTO)

        println("Concluiu Controller")

    }


}