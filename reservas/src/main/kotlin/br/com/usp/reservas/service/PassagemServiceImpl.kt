package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.PassagemEntradaDTO
import br.com.usp.reservas.dataclass.PassagemViewModel
import br.com.usp.reservas.mapper.PassagemEntradaMapper
import br.com.usp.reservas.mapper.PassagemSaidaMapper
import br.com.usp.reservas.repository.PassagemRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PassagemServiceImpl: PassagemService{

    @Autowired
    private lateinit var passagemRepository: PassagemRepository

    override fun consultar(passagemEntradaDTO: PassagemEntradaDTO): PassagemViewModel {
        val params = PassagemEntradaMapper.from(passagemEntradaDTO)
        val flightOffersSearches  = passagemRepository.consultar(params)
        return PassagemSaidaMapper.from(flightOffersSearches )
    }

}