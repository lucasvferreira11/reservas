package br.com.usp.reservas.controller

import br.com.usp.reservas.dataclass.MoedaEntradaDTO
import br.com.usp.reservas.dataclass.MoedaViewModel
import br.com.usp.reservas.service.MoedaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/moeda")
class MoedaController{

    @Autowired
    private lateinit var moedaService: MoedaService

    @GetMapping("/consultar")
    fun consultar(@RequestParam moeda_local: String, @RequestParam moeda_pretendida: String, @RequestParam valor_cotacao: String): MoedaViewModel {
        var moedaEntradaDTO =  MoedaEntradaDTO(moeda_local,moeda_pretendida,valor_cotacao)
        return moedaService.consultar(moedaEntradaDTO)
    }

}
