package br.com.usp.reservas.controller

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.Clima
import br.com.usp.reservas.dataclass.ClimaRetorno
import br.com.usp.reservas.service.ClimaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/clima")
class ClimaController{

    @Autowired
    private lateinit var climaService: ClimaService

    @GetMapping("/consultaClima")
    fun consultaClima(@RequestParam cidade: String, @RequestParam estado: String, @RequestParam chave: String): ClimaRetorno {
        val cidadeDTO = CidadeDTO(cidade,estado,chave)
        return climaService.consultar(cidadeDTO)
    }

}