package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.MoedaEntradaDTO
import br.com.usp.reservas.dataclass.MoedaViewModel

interface MoedaService{
    fun consultar(moedaEntradaDTO: MoedaEntradaDTO):MoedaViewModel
}