package br.com.usp.reservas.dataclass

data class PassagemEntradaDTO(
        val origem: String, val destino:String, val inicio: String, val fim: String, val preco: Double, val adultos: Int
)