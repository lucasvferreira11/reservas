package br.com.usp.reservas.repository

import br.com.usp.reservas.dataclass.ClimaRetorno
import br.com.usp.reservas.dataclass.MoedaSaidaDTO
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import javax.xml.ws.Response

@Repository
class MoedaRepository {

    companion object {
        const val URL = "https://economia.awesomeapi.com.br//"
    }

    fun consultar(moeda: String): ResponseEntity<Array<MoedaSaidaDTO>>{
        return RestTemplate().getForEntity(getUrl(moeda),Array<MoedaSaidaDTO>::class.java)
    }

    private fun getUrl(moeda: String): String{
        return URL + moeda
    }

}