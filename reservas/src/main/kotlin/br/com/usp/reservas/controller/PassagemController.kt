package br.com.usp.reservas.controller

import br.com.usp.reservas.dataclass.PassagemEntradaDTO
import br.com.usp.reservas.dataclass.PassagemViewModel
import br.com.usp.reservas.service.ClimaService
import br.com.usp.reservas.service.PassagemService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/passagem")
class PassagemController{

    @Autowired
    private lateinit var passagemService: PassagemService

    @PostMapping("/consultar")
    fun consultar(@RequestBody passagemEntradaDTO: PassagemEntradaDTO): PassagemViewModel{

        println("Entrou no Controller")
        return passagemService.consultar(passagemEntradaDTO)
        println("Concluiu Controller")

    }


}