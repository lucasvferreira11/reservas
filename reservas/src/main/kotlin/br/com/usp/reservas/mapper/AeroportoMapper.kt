package br.com.usp.reservas.mapper

class AeroportoMapper {

    companion object{

        private val map = hashMapOf<String, String>()

        init{
            map["São Paulo"] = "SAO"
            map["Bangkok"] = "BKK"
            map["Sydney"] = "SYD"
            map["Rio de Janeiro"] = "RIO"
        }

        fun from(aeroporto: String):String{
            return map.getOrDefault(aeroporto,"")
        }

    }
}