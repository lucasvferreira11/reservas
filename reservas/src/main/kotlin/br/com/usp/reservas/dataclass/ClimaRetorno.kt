package br.com.usp.reservas.dataclass

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ClimaRetorno(var temp: Int, var description: String, var currently: String,
                        var humidity: String, var wind_speedy: String, var city_name: String){
    constructor(): this(0,"","","","",""){

    }
}