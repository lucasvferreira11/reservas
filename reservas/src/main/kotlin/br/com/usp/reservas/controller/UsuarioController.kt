package br.com.usp.reservas.controller

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.Usuario
import br.com.usp.reservas.service.ClimaService
import br.com.usp.reservas.service.UsuarioService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/usuario")
class UsuarioController{

    @Autowired
    private lateinit var usuarioService: UsuarioService

    @PostMapping("/cadastrar")
    fun cadastrar(@RequestBody usuario: Usuario){
        usuarioService.cadastrar(usuario)
    }

    @GetMapping("/consultar")
    fun consultar(@RequestParam email: String): Usuario{
        return usuarioService.consultar(email)
    }

    @GetMapping("/validaSenha")
    fun consultar(@RequestParam email: String, @RequestParam senha: String): Boolean{
        return usuarioService.validaSenha(email,senha)
    }

}