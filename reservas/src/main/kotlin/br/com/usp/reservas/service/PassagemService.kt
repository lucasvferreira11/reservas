package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.PassagemEntradaDTO
import br.com.usp.reservas.dataclass.PassagemViewModel

interface PassagemService{
    fun consultar(passagemEntradaDTO: PassagemEntradaDTO): PassagemViewModel
}