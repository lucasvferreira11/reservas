package br.com.usp.reservas.repository

import br.com.usp.reservas.dataclass.Usuario
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface  UsuarioRepository: JpaRepository<Usuario,Long>{
    fun findByEmail(email: String): Usuario?
}