package br.com.usp.reservas.util

import java.text.DecimalFormat

class MoedaParser {

    companion object {
        fun calculaCotacao(cotacaoA: Double, cotacaoB: Double, valor: String): String{
            return calculaCotacao(cotacaoA,cotacaoB,valor.toDouble())
        }
        fun calculaCotacao(cotacaoA: Double, cotacaoB: Double, valor: Double): String{
            var df = DecimalFormat("#,###.00");
            return df.format(cotacaoA.div(cotacaoB) * valor)
        }

    }

}