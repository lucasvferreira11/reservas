package br.com.usp.reservas.dataclass

data class CidadeDTO(val cidade: String,val estado: String, val chave: String){

    override fun toString(): String {
        return cidade + "," + estado;
    }

}