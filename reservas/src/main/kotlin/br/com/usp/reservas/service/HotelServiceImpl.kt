package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.BaseViewModel
import br.com.usp.reservas.dataclass.HotelEntradaDTO
import br.com.usp.reservas.mapper.HotelEntradaMapper
import br.com.usp.reservas.mapper.HotelSaidaMapper
import br.com.usp.reservas.repository.HotelRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class HotelServiceImpl: HotelService{

    @Autowired
    private lateinit var hotelRepository: HotelRepository

    override fun consultar(hotelEntradaDTO: HotelEntradaDTO): List<BaseViewModel> {
        val params = HotelEntradaMapper.from(hotelEntradaDTO)
        val hotelOffers  = hotelRepository.consultar(params)
        return HotelSaidaMapper.from(hotelOffers, hotelEntradaDTO)
    }

}