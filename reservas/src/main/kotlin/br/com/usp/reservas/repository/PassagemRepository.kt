package br.com.usp.reservas.repository

import br.com.usp.reservas.consts.RepoConsts
import br.com.usp.reservas.dataclass.PassagemSaidaDTO
import com.amadeus.Amadeus
import com.amadeus.Params
import com.amadeus.resources.FlightOfferSearch
import org.springframework.stereotype.Repository

@Repository
class PassagemRepository{

    fun consultar(params: Params):List<FlightOfferSearch>{
        val amadeus = Amadeus.builder(RepoConsts.ID,RepoConsts.SECRET).build();
        return amadeus.shopping.flightOffersSearch.get(params).toList()
    }


}