package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.Clima
import br.com.usp.reservas.dataclass.ClimaRetorno
import br.com.usp.reservas.repository.ClimaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ClimaServiceImpl: ClimaService{

    @Autowired
    private lateinit var climaRepository: ClimaRepository

    override fun consultar(cidadeDTO: CidadeDTO): ClimaRetorno {
        var climaRetorno = climaRepository.consultar(cidadeDTO)
        return climaRetorno ?: ClimaRetorno()
    }


}