package br.com.usp.reservas.mapper

import br.com.usp.reservas.dataclass.HotelEntradaDTO
import br.com.usp.reservas.util.DataHelper
import com.amadeus.Params

class HotelEntradaMapper{

    companion object{
        fun from(hotelEntradaDTO: HotelEntradaDTO): Params {
            return Params.with("cityCode", hotelEntradaDTO.cidade)
                    .and("adults", hotelEntradaDTO.adulto)
                    .and("checkInDate",hotelEntradaDTO.checkIn)
                    .and("checkOutDate",hotelEntradaDTO.checkOut)
                    .and("currency", "USD")
                    .and("priceRange", getPriceRangeAsString(hotelEntradaDTO.preco))
        }

        private fun getPriceRangeAsString(preco: Int): String {
            return "0-" + preco
        }
    }

}