package br.com.usp.reservas.service

import br.com.usp.reservas.dataclass.CidadeDTO
import br.com.usp.reservas.dataclass.ClimaRetorno

interface ClimaService{
    fun consultar(cidadeDTO: CidadeDTO): ClimaRetorno
}